describe('Create a request', () => {
    it('Will send a form to the server', () => {
        cy.visit('http://localhost:3000');

        cy.get('[data-testid="btnReset"]')
            .should('be.disabled');

        cy.get('[data-testid="btnSave"]')
            .should('be.disabled');

        cy.get('[data-testid="txtFromLocalization"]')
            .type('Bruxelles')
            .should('have.value', 'Bruxelles');

        cy.get('[data-testid="btnReset"]')
            .should('be.enabled');

        cy.get('[data-testid="btnSave"]')
            .should('be.enabled');

        cy.get('[data-testid="areaComboBox"]')
            .should('not.be.visible');

        cy.get('[data-testid="txtToLocalization"]')
            .type('Paris')
            .should('have.value', 'Paris');

        cy.get('[data-testid="areaComboBox"]')
            .should('be.visible');

        cy.get('[data-testid="cbCar"]')
            .check().should('be.checked');
        cy.get('[data-testid="cbTrain"]')
            .check().should('be.checked');
        cy.get('[data-testid="cbBike"]')
            .check().should('be.checked');
        cy.get('[data-testid="cbCar"]')
            .uncheck().should('not.be.checked');

        cy.get('[data-testid="areaFilter"]')
            .should('be.visible');

        cy.get('[data-testid="cbFilterBar"]')
            .check({force: true}).should('be.checked');
        cy.get('[data-testid="cbFilterRestaurant"]')
            .check({force: true}).should('be.checked');

        cy.get('[data-testid="txtBudget"]')
            .type('350')
            .should('have.value', '350');

        cy.get('[data-testid="dateStartDate"]')
            .type('2019-05-22');

        cy.get('[data-testid="btnSend"]').click();
    });
});