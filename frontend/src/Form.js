import React from 'react';
import './Form.css';

class Form extends React.Component {
    constructor() {
        super();
        this.state = {
            fromLocalization: '',
            toLocalization: '',
        };
    }

    handleSubmit = () => {
        alert('Submit');
    }

    resetField = () => {
        alert('resetField');
    }

    saveSearch = () => {
        alert('SaveSearch');
    }

    send = () => {
        
    }
//AIzaSyByESFmtdgavIhfhHOAqdJzfF90NuFyv6o

    handleFromLocalizationChange = (evt) => {
        this.setState({ fromLocalization: evt.target.value });
    }

    handleToLocalizationChange = (evt) => {
        this.setState({ toLocalization: evt.target.value });
    }

    handleCheckCarClick = () => {
        this.setState({ checked: !this.state.checkedCbCar });
    }

    handleCheckTrainClick = () => {
        this.setState({ checked: !this.state.checkedCbTrain });
    }

    handleCheckBusClick = () => {
        this.setState({ checked: !this.state.checkedCbBus });
    }

    handleCheckBikeClick = () => {
        this.setState({ checked: !this.state.checkedCbBike });
    }
    handleCheckFilterBarClick = () => {
        this.setState({ checked: !this.state.checkedCbFilterBar });
    }
    handleCheckFilterRestaurantClick = () => {
        this.setState({ checked: !this.state.checkedCbFilterRestaurant });
    }
    handleCheckFilterSportClick = () => {
        this.setState({ checked: !this.state.checkedCbFilterSport });
    }
    handleCheckFilterCulturalClick = () => {
        this.setState({ checked: !this.state.checkedCbFilterCultural });
    }
    handleCheckFilterHotelClick = () => {
        this.setState({ checked: !this.state.checkedCbFilterHotel });
    }

    handleDateChange = (evt) => {
        this.setState({startDate: evt.target.value})
    }

    handleBudgetChange = (evt) => {
        this.setState({budget: evt.target.value})
    }

    render() {
        const { fromLocalization, toLocalization, checkedCbCar, checkedCbTrain, checkedCbBus, checkedCbBike,
            checkedCbFilterBar, checkedCbFilterRestaurant, checkedCbFilterSport, checkedCbFilterCultural, checkedCbFilterHotel, startDate, budget } = this.state;
        const enabled = fromLocalization.length != 0;
        const isCbTransportVisible = fromLocalization.length != 0 && toLocalization.length != 0;
        return (
            <form onSubmit={this.handleSubmit}>
                <div>
                    <button
                        data-testid="btnReset"
                        disabled={!enabled}
                        onClick={this.resetField}>
                        Reset
                </button>
                    <button
                        data-testid="btnSave"
                        disabled={!enabled}
                        onClick={this.saveSearch}>
                        Save
                </button>
                </div>
                <div>
                    <label>
                        From :
                        <input
                            data-testid="txtFromLocalization"
                            placeholder="Enter a localization"
                            value={this.state.fromLocalization}
                            onChange={this.handleFromLocalizationChange}
                        />
                    </label>
                </div>
                <div>
                    <label>
                        To :
                        <input
                            data-testid="txtToLocalization"
                            placeholder="Enter a localization"
                            value={this.state.toLocalization}
                            onChange={this.handleToLocalizationChange}
                        />
                    </label>
                </div>
                {isCbTransportVisible && (
                    <div data-testid="areaComboBox">
                        <label>
                            <input
                                type="checkbox"
                                data-testid="cbCar"
                                checked={this.state.checkedCbCar}
                                onChange={this.handleCheckCarClick}
                            />Car</label>
                        <label>
                            <input
                                type="checkbox"
                                data-testid="cbTrain"
                                checked={this.state.checkedCbTrain}
                                onChange={this.handleCheckTrainClick}
                            />Train</label>
                        <label>
                            <input
                                type="checkbox"
                                data-testid="cbBus"
                                checked={this.state.checkedCbBus}
                                onChange={this.handleCheckBusClick}
                            />Bus</label>
                        <label>
                            <input
                                type="checkbox"
                                data-testid="cbBike"
                                checked={this.state.checkedCbBike}
                                onChange={this.handleCheckBikeClick}
                            />Bike</label>
                    </div>
                )}
                <div>
                    <div id="areaFilter" data-testid="areaFilter">
                        <label><input
                            type="checkbox"
                            data-testid="cbFilterBar"
                            checked={this.state.checkedCbFilterBar}
                            onChange={this.handleCheckBikeClick}
                        ></input><span>Bar</span></label>
                        <label><input
                            type="checkbox"
                            data-testid="cbFilterRestaurant"
                            checked={this.state.checkedCbFilterBar}
                            onChange={this.handleCheckBikeClick}
                        ></input><span>Restaurant</span></label>
                        <label><input
                            type="checkbox"
                            data-testid="cbFilterSport"
                            checked={this.state.checkedCbFilterBar}
                            onChange={this.handleCheckBikeClick}
                        ></input><span>Sport</span></label>
                        <label><input
                            type="checkbox"
                            data-testid="cbFilterCultural"
                            checked={this.state.checkedCbFilterBar}
                            onChange={this.handleCheckBikeClick}
                        ></input><span>Cultural</span></label>
                        <label><input
                            type="checkbox"
                            data-testid="cbFilterHotel"
                            checked={this.state.checkedCbFilterBar}
                            onChange={this.handleCheckBikeClick}
                        ></input><span>Hotel</span></label>
                    </div>
                </div>
                <div id="test">
                    <label>
                        Budget :
                        <input
                            data-testid="txtBudget"
                            placeholder="Enter a Budget"
                            value={this.state.budget}
                            onChange={this.handleBudgetChange}
                        />
                    </label><span>$ (Max)</span>
                </div>
                <div>
                    <label>
                        From :
                        <input
                            data-testid="dateStartDate"
                            type="date"
                            value={this.state.startDate}
                            onChange={this.handleDateChange}
                        />
                    </label>
                </div>
                <div>
                <button
                        data-testid="btnSend"
                        disabled={!enabled}
                        onClick={this.send}>
                        Save
                </button>
                </div>
            </form>
        )
    }
}

export default Form;

class LoginFrm extends React.Component {
    constructor() {
        super();
        this.state = {
            email: '',
            password: '',
        };
    }

    handleEmailChange = (evt) => {
        this.setState({ email: evt.target.value });
    }

    handlePasswordChange = (evt) => {
        this.setState({ password: evt.target.value });
    }

    handleSubmit = () => {
        const { email, password } = this.state;
        alert(`Welcome ${email} password: ${password}`);
    }

    render() {
        const { email, password } = this.state;
        const enabled =
            email.length > 0 &&
            password.length > 0;
        return (
            <form onSubmit={this.handleSubmit}>
                <input
                    type="text"
                    placeholder="Email"
                    value={this.state.email}
                    onChange={this.handleEmailChange}
                />

                <input
                    type="password"
                    placeholder="Password"
                    value={this.state.password}
                    onChange={this.handlePasswordChange}
                />
                <button disabled={!enabled}>Login</button>
            </form>
        )
    }
}
