import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';

const AnyReactComponent = ({ text }) => (<div>{text}</div>);

class SimpleMap extends Component {
  static defaultProps = {
    center: {
      lat: 59.95,
      lng: 30.33
    },
    zoom: 11
  };

  render() {
    return (
      // Important! Always set the container height explicitly
      <div style={{ height: '500px', width: '70%' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: 'AIzaSyCp2oDTycJrb99zvI3ULdjbJ5Y_goC_65Q' }}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
        >
          <AnyReactComponent
            lat={59.955413}
            lng={30.337844}
            text="My Marker"
          />
          <AnyReactComponent
            lat={60.955413}
            lng={31.337844}
            text="My second Marker"
          />
          <AnyReactComponent
            lat={58.955413}
            lng={29.337844}
            text="My 3rd Marker"
          />
        </GoogleMapReact>
      </div>
    );
  }
}

export default SimpleMap;