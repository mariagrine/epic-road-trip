import React from 'react';
import Form from './Form';
import SimpleMap from './Map';

function App() {
  return (
    <div>
        <Form/>
        <SimpleMap/>
    </div>
  );
}

export default App;
